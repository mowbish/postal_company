from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from common.basemodels import BaseModel
from conf import settings
from geopy.distance import great_circle
import paho.mqtt.client as mqtt



class User(AbstractUser):
    phone_number = models.CharField(max_length=20, verbose_name=_('Phone Number'), blank=True,
                                    error_messages={
                                        "unique": _("A user with that phone number already exists.")})
    address = models.TextField(verbose_name=_('Address'), blank=True)


class Driver(BaseModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    license_number = models.CharField(max_length=50, verbose_name=_('License Number'))
    vehicle_model = models.CharField(max_length=50, verbose_name=_('Vehicle Model'))
    current_location_latitude = models.FloatField(verbose_name=_('Current Location Latitude'), null=True, blank=True)
    current_location_longitude = models.FloatField(verbose_name=_('Current Location Longitude'), null=True, blank=True)
    ready_for_mission = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Mission(BaseModel):
    PICKUP = 'P'
    DELIVERY = 'D'
    MISSION_TYPE_CHOICES = [
        (PICKUP, _('Pickup')),
        (DELIVERY, _('Delivery')),
    ]
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE, verbose_name=_('Driver'), null=True, blank=True)
    source_address = models.TextField(verbose_name=_('Source Address'))
    destination_address = models.TextField(verbose_name=_('Destination Address'))
    is_completed = models.BooleanField(default=False, verbose_name=_('Is Completed'))
    pickup_time = models.DateTimeField(verbose_name=_('Pickup Time'))
    delivery_time = models.DateTimeField(verbose_name=_('Delivery Time'))
    mission_type = models.CharField(
        max_length=1,
        choices=MISSION_TYPE_CHOICES,
        verbose_name=_('Mission Type')
    )


    def __str__(self):
        mission_type = next(item[1] for item in self.MISSION_TYPE_CHOICES if item[0] == self.mission_type)
        return f"{mission_type} - {self.driver}"
    
    def assign_driver(self):
        nearest_driver = self.calculate_nearest_driver()

        if nearest_driver.ready_for_mission:
            self.driver = nearest_driver
            self.save()
            nearest_driver.ready_for_mission = False
            nearest_driver.save()

    
    def calculate_nearest_driver(self):
        nearest_driver = None
        min_distance = float("inf")

        def on_message(client, userdata, msg):
            driver_location_data = msg.payload.decode()
            driver_id = msg.topic.split("/")[-1]
            latitude, longitude = extract_latitude_longitude(driver_location_data)
            distance = self.calculate_distance(latitude, longitude, self.source_lat, self.source_long)
            nonlocal min_distance, nearest_driver

            if distance < min_distance and not Driver.objects.filter(pk=driver_id, ready_for_mission=False).exists():
                min_distance = distance
                nearest_driver = driver_id

        client = mqtt.Client()
        client.on_message = on_message
        client.connect("localhost", 1883)
        client.subscribe("/test/driver/+")
        client.loop_start()

        # wait for get messages
        while nearest_driver is None:
            pass

        client.loop_stop()
        client.disconnect()
        driver = Driver.objects.get(pk=nearest_driver)
        return driver

    def calculate_distance(self, source_lat, source_long, driver_lat, driver_long):
        source_point = (source_lat, source_long)
        driver_point = (driver_lat, driver_long)

        distance = great_circle(source_point, driver_point).kilometers
        return distance

def extract_latitude_longitude(data):
    # extract geo lat and long 
    latitude = ...
    longitude = ...
    return latitude, longitude
