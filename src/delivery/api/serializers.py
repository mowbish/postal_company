from rest_framework import serializers, exceptions
from delivery.models import User, Driver, Mission
from django.contrib.auth.hashers import make_password


class SignUpUserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email", "password", "phone_number", "address", "is_staff")
        
    def create(self, validated_data):

        user = User.objects.create(
            username=validated_data["username"],
            first_name=validated_data["first_name"],
            last_name=validated_data["last_name"],
            email=validated_data["email"],
            phone_number=validated_data["phone_number"],
            address=validated_data["address"],
            is_staff=validated_data["is_staff"]
        )
        user.set_password(raw_password=validated_data["password"])
        user.save()
        return user

    def validate(self, attrs):
        """
        i use unique validation error in serializer validate method,
        how ever it was in AbstractUser model; but i want, if the request
        has a mistake or problem, it will be resolve in serializer.
        and a bad request is won't go to models.
        """

        for field in self.Meta.fields:
            if field not in attrs:
                raise serializers.ValidationError(f"{field} field is required.")

        if User.objects.filter(username=attrs['username']).exists():
            raise serializers.ValidationError("This Username already taken")
        elif User.objects.filter(email=attrs['email']).exists():
            raise serializers.ValidationError("This email already taken")
        elif User.objects.filter(phone_number=attrs['phone_number']).exists():
            raise serializers.ValidationError("This phone number already taken")

        return super().validate(attrs)


class UserDetailModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "email")


class DestroyUserModelSerializer(serializers.ModelSerializer):
    """
        The user cannot delete his/her account completely
        user only can deactive himself/herself account.
    """

    class Meta:
        model = User
        fields = ("username", "password")

    def update(self, instance):
        if instance.is_active != False:
            instance.is_active = False
            instance.save()
        return instance

    def validate(self, attrs):
        requested_user = self.context['request'].user
        password = make_password(attrs['password'])
        if requested_user.username != attrs["username"]:
            raise PermissionError("You dont have access to this account")
        elif not User.objects.filter(username=requested_user.username).exists():
            raise ProcessLookupError("user not found")
        elif User.objects.filter(username=requested_user.username).first().password != password:
            raise LookupError("username or password may wrong")
        return super().validate(attrs)

    def to_representation(self, instance):
        return {"status": "your account deleted successfully"}


class DriverModelSerializer(serializers.ModelSerializer):
    user = UserDetailModelSerializer()
    class Meta:
        model = Driver
        fields = ("user", "license_number", "vehicle_model")
    
        user = UserDetailModelSerializer(read_only=True)

    class Meta:
        model = Driver
        fields = ("user", "license_number", "vehicle_model")

    def create(self, validated_data):
        user_data = validated_data.pop("user")
        user_serializer = UserDetailModelSerializer(data=user_data)
        user_serializer.is_valid(raise_exception=True)
        user = user_serializer.save()

        driver = Driver.objects.create(user=user, **validated_data)

        return driver

    def validate(self, attrs):
        """
        i use unique validation error in serializer validate method,
        how ever it was in AbstractUser model; but i want, if the request
        has a mistake or problem, it will be resolve in serializer.
        and a bad request is won't go to models.
        """

        for field in self.Meta.fields:
            if field not in attrs:
                raise serializers.ValidationError(f"{field} field is required.")

        return super().validate(attrs)


class MissionModelSerializer(serializers.ModelSerializer):
    
    # driver = DriverModelSerializer(read_only=True)
    class Meta:
        model = Mission
        fields = ("source_address", "destination_address", "is_completed",
                  "pickup_time", "delivery_time", "mission_type")
    
    def validate(self, attrs):
        """
        i use unique validation error in serializer validate method,
        how ever it was in AbstractUser model; but i want, if the request
        has a mistake or problem, it will be resolve in serializer.
        and a bad request is won't go to models.
        """

        for field in self.Meta.fields:
            if field not in attrs:
                raise serializers.ValidationError(f"{field} field is required.")
   
        return super().validate(attrs)
    