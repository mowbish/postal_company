from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from delivery.models import User, Driver, Mission
from .serializers import (
    SignUpUserModelSerializer, UserDetailModelSerializer,
    DestroyUserModelSerializer, DriverModelSerializer,
    MissionModelSerializer)
from .permissions import IsOwner



class UserModelViewSet(mixins.UpdateModelMixin,
                       mixins.RetrieveModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    lookup_field = "username"
    permission_classes = [IsOwner]

    def get_queryset(self):
        return User.objects.filter(username=self.request.user.username, is_active=True)

    def get_serializer_class(self):

        if self.action == "destroy":
            self.serializer_class = DestroyUserModelSerializer
        else:
            self.serializer_class = UserDetailModelSerializer

        return self.serializer_class

    @action(detail=False, methods=["POST"], permission_classes=[AllowAny], url_path="sign-up")
    def sign_up(self, request):
        """
            with this sign-up action every body can create account
        """

        serializer = SignUpUserModelSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"data": "Your account created successfully."})

    @action(detail=False, methods=["POST"], permission_classes=[AllowAny], url_path="admin/sign-up")
    def admin_sign_up(self, request):
        """
            with this sign-up action only admins can create account
        """

        serializer = SignUpUserModelSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"data": "Your admin account created successfully."})

    @action(detail=False, methods=["GET"], permission_classes=[IsAdminUser], url_path="all")
    def get_all_users(self, request):
        """
            Retrieve a list of all users.
            Only accessible by admins.
        """
        queryset = User.objects.all()
        serializer = UserDetailModelSerializer(queryset, many=True)
        return Response(serializer.data)


class DriverModelViewSet(mixins.UpdateModelMixin,
                         mixins.RetrieveModelMixin, mixins.DestroyModelMixin,
                         mixins.ListModelMixin, viewsets.GenericViewSet):
    lookup_field = "username"
    permission_classes = [IsAdminUser]

    def get_queryset(self):
        return Driver.objects.filter(user__is_active=True)

    def get_serializer_class(self):

        if self.action == "destroy":
            self.serializer_class = DestroyUserModelSerializer
        else:
            self.serializer_class = DriverModelSerializer

        return self.serializer_class
    
    @action(detail=False, methods=["POST"], permission_classes=[IsAdminUser], url_path="create")
    def create_driver(self, request):        
        serializer = DriverModelSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        driver = serializer.save(current_location_latitude=None, current_location_longitude=None, ready_for_mission=True)
        return Response({"data": "Driver account created successfully.", "id": driver.id})

    @action(detail=True, methods=["GET"], permission_classes=[IsAdminUser], url_path="missions")
    def show_missions_of_driver(self, request, driver_id=None):
        """
            Retrieve the missions of a driver
        """
        try:
            driver = Driver.objects.get(id=driver_id)
            missions = Mission.objects.filter(driver=driver)
            serializer = MissionModelSerializer(missions, many=True)
            return Response(serializer.data)
        except Driver.DoesNotExist:
            return Response({"error": "Driver not found"}, status=404)


class MissionModelViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin,
                         mixins.RetrieveModelMixin, mixins.DestroyModelMixin,
                         mixins.ListModelMixin, viewsets.GenericViewSet):
    lookup_field = "id"
    permission_classes = [IsAdminUser]
    
    def get_queryset(self):
        return Mission.objects.all
    
    def get_serializer_class(self):

        self.serializer_class = MissionModelSerializer
        return self.serializer_class
        
    
    @action(detail=False, methods=["PUT"], permission_classes=[AllowAny], url_path="automatic-assign")
    def assign_automatically(self, request, pk=None):
        
        mission = self.get_object()
        mission.assign_driver()  # Automatically assign driver using calculate_nearest_driver()
        
        return Response({"message": "Mission assigned successfully"})


    @action(detail=True, methods=["PUT"], permission_classes=[IsAdminUser], url_path="manual-assign")
    def assign_manually(self, request, pk=None, driver_id=None):
        
        mission = self.get_object()
        driver = Driver.objects.get(id=driver_id)
        mission.driver = driver
        mission.save()  # Manually assign mission to a specific driver

        return Response({"message": "Mission assigned successfully"})
