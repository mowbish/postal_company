from rest_framework.routers import DefaultRouter
from .views import UserModelViewSet, DriverModelViewSet, MissionModelViewSet

router = DefaultRouter()

router.register(r'user', UserModelViewSet,
                basename='user')
router.register(r'driver', DriverModelViewSet,
                basename='driver')
router.register(r'mission', MissionModelViewSet,
                basename='mission')
