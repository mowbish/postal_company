from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from delivery.models import User, Driver, Mission

class MissionAPITestCase(APITestCase):

    def setUp(self):

        self.normal_user = User.objects.create_user(
            username='user1',
            password='user123',
            email='user1@example.com',
            phone_number='1234567890',
            address='User Address'
        )


        self.admin_user = User.objects.create_user(
            username='admin',
            password='admin123',
            email='admin@example.com',
            phone_number='0987654321',
            address='Admin Address',
            is_staff=True
        )


        self.client.login(username='admin', password='admin123')        
        self.client.login(username='user1', password='user123')
    
    def test_login(self):
        admin_authenticated = self.client.session.get('authenticated', True)
        user_authenticated = self.client.session.get('authenticated', True)

        self.assertTrue(admin_authenticated)
        self.assertTrue(user_authenticated)

    def test_create_driver(self):
        self.client.login(username='admin', password='admin123')
        url = "http://localhost:8000/api/driver/"
        data = {
            'user': self.normal_user.id,
            'license_number': 'ABC123',
            'vehicle_model': 'Car',
            'current_location_latitude': 40.7128,
            'current_location_longitude': -74.0060,
            'ready_for_mission': True
        }
        response = self.client.post(url, data)
        print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Driver.objects.count(), 1)

    # def test_create_mission(self):

    #     url = reverse('mission-list')
    #     data = {
    #         'driver': driver_id,
    #         'source_address': 'Source Address',
    #         'destination_address': 'Destination Address',
    #         'is_completed': False,
    #         'pickup_time': '2023-06-23T10:00:00Z',
    #         'delivery_time': '2023-06-23T12:00:00Z',
    #         'mission_type': 'P'
    #     }
    #     response = self.client.post(url, data)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(Mission.objects.count(), 1)

    # def test_list_missions_and_drivers(self):

    #     url = reverse('mission-list')
    #     response = self.client.get(url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), 1)

    #     url = reverse('driver-list')
    #     response = self.client.get(url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), 1)

    # def test_assign_mission_to_driver(self):

    #     driver = Driver.objects.create(
    #         user=self.normal_user,
    #         license_number='ABC123',
    #         vehicle_model='Car',
    #         current_location_latitude=40.7128,
    #         current_location_longitude=-74.0060,
    #         ready_for_mission=True
    #     )


    #     mission = Mission.objects.create(
    #         driver=driver,
    #         source_address='Source Address',
    #         destination_address='Destination Address',
    #         is_completed=False,
    #         pickup_time='2023-06-23T10:00:00Z',
    #         delivery_time='2023-06-23T12:00:00Z',
    #         mission_type='P'
    #     )


    #     url = reverse('mission-assign', args=[mission.id, driver.id])
    #     response = self.client.put(url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertTrue(mission.driver == driver)
