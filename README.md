# postal_company

[![python](https://img.icons8.com/color/48/000000/python.png)](https://www.python.org/)
[![django](https://img.icons8.com/color/48/000000/django.png)](https://www.djangoproject.com/)
[![Docker](https://img.icons8.com/color/48/000000/docker.png)](https://www.docker.com/)
[![DRF](https://img.icons8.com/color/48/000000/api.png)](https://www.django-rest-framework.org/)


## Installation
---

### Docker

First of all clone repo:

```shell
git clone https://github.com/mowbish/postal_company.git
```

next step: cd to cloned project in terminal or cmd:

| Windows | Linux |
| --- | --- |
| ``> cd postal_company\  `` | ``$ cd postal_company/`` |

now you should enter this command (you should install docker and docker-compose before):

```shell
sudo docker-compose up --build
```

Wait a few minutes

well done! :D

you can see project in this route:

+ `http://127.0.0.1:8000/`

notic: Only the first time you need to hit `--build` in `docker-compose` command and after the first time it is no longer necessary

---

### Custom - if you don't want Docker

clone repo:

```shell
git clone https://github.com/mowbish/postal_company.git
```

<br>
<br>

### Also Rout's of project:

+ `http://127.0.0.1:8000/`
+ `http://127.0.0.1:8000/swagger/`

Enjoy it ;)
<br>

#### Project Structure
---

```shell

   ├── Dockerfile
   ├── requirements.txt
   └── src
       ├── delivery
       │   ├── admin.py
       │   ├── api
       │   │   ├── permissions.py
       │   │   ├── serializers.py
       │   │   ├── urls.py
       │   │   └── views.py
       │   │
       │   ├── apps.py
       │   ├── __init__.py
       │   ├── migrations
       │   │   └── __init__.py
       │   │
       │   ├── models.py
       │   ├── tests.py
       │   └── views.py
       │
       ├── common
       │   └── basemodels.py
       │
       ├── conf
       │   ├── asgi.py
       │   ├── __init__.py
       │   ├── settings.py
       │   ├── urls.py
       │   └── wsgi.py
       │
       └── manage.py
